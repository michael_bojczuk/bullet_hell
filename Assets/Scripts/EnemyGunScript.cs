﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGunScript : MonoBehaviour {

    public float reloadSpeed;
    public GameObject bulletObject;
    private GameObject[] bulletArray;


    private float reloadTimer;
    private int count;

	// Use this for initialization
	void Start () {
        reloadTimer = reloadSpeed;
        bulletArray = new GameObject[15];
        for(int i = 0; i < bulletArray.Length; i++)
        {
            bulletArray[i] = Instantiate(bulletObject);
            bulletArray[i].transform.position = new Vector3(10, 10, 0);
        }
        count = 0;
	}
	
	// Update is called once per frame
	void Update () {
        reloadTimer -= Time.deltaTime;
        if(reloadTimer <= 0)
        {
            reloadTimer = reloadSpeed;
            bulletArray[count].transform.position = this.transform.position;
            bulletArray[count].transform.rotation = this.transform.rotation;
            bulletArray[count].GetComponent<EnemyBullet>().spawningBullet();
            count++;
            if(count >= bulletArray.Length)
            {
                count = 0;
            }
        }
	}
}
