﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

	public string powerUpColour;
    public float fallSpeed;

	// Use this for initialization
	void Start () {
		
	}



	// Update is called once per frame
	void Update () {
        this.transform.position = this.transform.position + Vector3.down * Time.deltaTime * fallSpeed;
	}
}
