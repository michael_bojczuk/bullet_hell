﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class background : MonoBehaviour {

    public float speed = 0.01f;
    private Material _material;
    void Start()
    {
        _material = GetComponent<Renderer>().material;
        _material.mainTexture.wrapMode = TextureWrapMode.Repeat;
    }

    void Update()
    {
        _material.mainTextureOffset = new Vector2(0, Time.time * speed);
        //GetComponent<Renderer>().material.mainTextureOffset = offset;
    }
}
