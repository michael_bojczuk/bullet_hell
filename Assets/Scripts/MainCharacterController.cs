﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainCharacterController : MonoBehaviour {

    //Public vars
    public float accelerationFactor, maxSpeed;

    //Private vars
    private float verticalVel, horizontalVel;
    private float dt;
	private Vector3 moveVector;

    public GameObject PlayerExplosion;

	// Use this for initialization
	void Start () {
		
        verticalVel = 0;
        horizontalVel = 0;
        dt = 0;
	}
	

	// Update is called once per frame
	void Update () {

        //Time controls
        dt = Time.deltaTime;
	
        //Movement control
        //Vertical movement
        if(Input.GetAxis("Vertical") != 0)
        {
            verticalVel = Input.GetAxis("Vertical") * maxSpeed;  //Movement speed is adjusted as a percent of difference between current speed and the speed being aimed for.
        }
        else
        {
            verticalVel = 0;
        }

        //Horizontal movement
        if (Input.GetAxis("Horizontal") != 0)
        {
            horizontalVel = Input.GetAxis("Horizontal") * maxSpeed;   //Movement speed is adjusted as a percent of difference between current speed and the speed being aimed for.
        }
        else
        {
            horizontalVel = 0;
        }

        //Check character not moving off screen
        if(verticalVel*dt + this.transform.position.y <-4.5 || verticalVel * dt + this.transform.position.y >4.5)
        {
            verticalVel = 0;
        }
        if (horizontalVel * dt + this.transform.position.x < -8.5 || horizontalVel * dt + this.transform.position.x > 8.5)
        {
            horizontalVel = 0;
        }
        //Update character position
        moveVector = new Vector3(horizontalVel, verticalVel, 0); 
		moveVector = Vector3.ClampMagnitude (moveVector, maxSpeed);

        this.transform.position += moveVector *dt; 

    }

    public void charDie()
    { 
        //explosion of character
        Instantiate(PlayerExplosion, transform.position, transform.rotation);
        Score.score = 0;
        SceneManager.LoadScene(0);
    }


}
