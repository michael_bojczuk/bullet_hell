﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface gunInterface {

	void activate();
	void deactivate();
	void setSpeed(float speed);
    void stopShooting();
}
