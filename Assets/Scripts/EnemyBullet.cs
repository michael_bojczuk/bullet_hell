﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    public float bulletSpeed;
    public float bulletTime;
    public float bulletDamage;

    private bool spawned;
    private float spawnTime;

    // Use this for initialization
    void Start()
    {
        spawned = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (spawned)
        {
            this.transform.position += this.transform.rotation * Vector3.down * bulletSpeed * Time.deltaTime;
            spawnTime -= Time.deltaTime;
            if (spawnTime <= 0)
            {
                spawned = false;
            }
        }
    }

    public void spawningBullet()
    {
        spawned = true;
        spawnTime = bulletTime;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        other.GetComponent<MainCharacterController>().charDie();
        this.transform.position = new Vector3(100, 100, 0);
    }
}
