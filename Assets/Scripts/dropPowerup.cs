﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropPowerup : MonoBehaviour{

	public bool dropsPowerup;
	public GameObject powerUp;

	public void dropPowerUp()
	{
		GameObject temp = Instantiate (powerUp);
		temp.transform.position = this.transform.position;
	}

}
