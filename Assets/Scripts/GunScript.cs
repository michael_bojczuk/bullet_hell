﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour, gunInterface {

	//Public vars
	public float reloadTime;
	public GameObject bulletType;
	public int maxBullets;

    //Private vars
    private bool active;
	private bool shooting;
	private float cooldownTimer;
	private GameObject[] bulletArray;
	private int currentBullet;

    private bool shoot = false;

    

    // Use this for initialization
    void Start () {
        //audio
        

        active = false;
		shooting = false;
		bulletArray = new GameObject[maxBullets];
		for (int i = 0; i < maxBullets; i++) {
			bulletArray [i] = Instantiate (bulletType);
			bulletArray [i].transform.position = new Vector3 (10, 10, 0);
		}
		currentBullet = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (active)
        {
            shooting = Input.GetButton("Fire1");
            //bool for if but down and what not
            shoot = true;
        }
		if (cooldownTimer > 0) {
			cooldownTimer -= Time.deltaTime;
            
        } else {
			cooldownTimer = 0;
        }

	}



    void FixedUpdate()
	{
        AudioSource audio = GetComponent<AudioSource>();
        if (cooldownTimer == 0 && shooting) {
			bulletArray [currentBullet].transform.position = this.transform.position;
			bulletArray [currentBullet].transform.rotation = this.transform.rotation;
			bulletArray [currentBullet].GetComponent<BulletScript> ().spawned ();
			cooldownTimer = reloadTime;
            audio.Play();
            currentBullet++;
			if (currentBullet == maxBullets) {
				currentBullet = 0;
			}
		}
	}

    public void activate()
    {
        active = true;
        this.GetComponent<SpriteRenderer>().enabled = true;
    }

    public void deactivate()
    {
        active = false;
        this.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void setSpeed(float newSpeed)
    {
        reloadTime = newSpeed;
    }
		
    public void stopShooting()
    {
        shooting = false;
    }

}
