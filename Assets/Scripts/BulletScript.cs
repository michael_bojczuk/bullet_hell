﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	public float bulletSpeed;
	public float bulletTime;
	public float bulletDamage;

	private bool firing;
	private float spawnTime;

	// Use this for initialization
	void Start () {
		firing = false;
	}

	// Update is called once per frame
	void Update () {
		if (firing) {
			this.transform.position += this.transform.rotation * Vector3.up * bulletSpeed * Time.deltaTime;
		}
        if (this.transform.position.y >= 6.5)
        {
            this.transform.position = new Vector3(-10, -10, 0);
            firing = false;
        }
		if (spawnTime > 0) {
			spawnTime -= Time.deltaTime;
		} else {
            this.transform.position = new Vector3(-10, -10, 0);
			firing = false;
		}

	}

	public void spawned()
	{
		firing = true;
		spawnTime = bulletTime;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		print ("hit something " + other.name);
		other.GetComponent<alienSpawner> ().takeDamage(bulletDamage);
		this.transform.position = new Vector3 (100, 100, 0);
	}
}
