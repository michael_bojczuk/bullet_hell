﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeScript : MonoBehaviour {

	public GameObject[] guns;

	private gunInterface[] gunArray;
    private int yellowUpgradeNum;
    private int purpleUpgradeNum;
    private int purpleEquipped;
    private int yellowEquipped;
	private string recentUpgrade;
    private int maxUpgradeNum;

	// Use this for initialization
	void Start () {
        yellowUpgradeNum = 1;
        purpleUpgradeNum = 0;
        yellowEquipped = 0;
        purpleEquipped = 0;
        maxUpgradeNum = 3;
		gunArray = new gunInterface[guns.Length];
		recentUpgrade = "green";
		for (int i = 0; i < guns.Length; i++) {
			gunArray [i] = guns [i].GetComponent<gunInterface> ();
		}
	}
	
	// Update is called once per frame
	void Update () {



        if(yellowUpgradeNum > 0)
        {
			if (yellowEquipped != yellowUpgradeNum && recentUpgrade.CompareTo("green") == 0)
            {
                stopAllGuns();
				for (int i = 0; i < gunArray.Length; i++)
                {
                    gunArray[i].deactivate();
                }
                switch (yellowUpgradeNum)
                {
                    //Need to add in bullet speed changes.
					case 1:
						yellowEquipped = 1;
                        gunArray[0].activate();
                        for(int i = 0; i < 3; i++)
                        {
                            gunArray[i].setSpeed(0.1f);
                        }
                        break;
					case 2:
						yellowEquipped = 2;
                        gunArray[0].activate();
                        gunArray[1].activate();
                        gunArray[2].activate();
                        for (int i = 0; i < 3; i++)
                        {
                            gunArray[i].setSpeed(0.1f);
                        }
                        break;
					case 3:
						yellowEquipped = 3;
                        gunArray[0].activate();
                        gunArray[1].activate();
                        gunArray[2].activate();
                        for (int i = 0; i < 3; i++)
                        {
                            gunArray[i].setSpeed(0.05f);
                        }
                        break;
                }
            }
        }
        if(purpleUpgradeNum > 0)
        {
			if(purpleEquipped != purpleUpgradeNum && recentUpgrade.CompareTo("red") == 0)
            {
                stopAllGuns();
				for (int i = 0; i < gunArray.Length; i++)
                {
                    gunArray[i].deactivate();
                }
                switch (purpleUpgradeNum)
                {
                    //Need to add in bullet speed changes.
					case 1:
						purpleEquipped = 1;
                        gunArray[3].activate();
                        break;
					case 2:
						purpleEquipped = 2;
                        gunArray[3].activate();
                        break;
					case 3:
						purpleEquipped = 3;
	                    gunArray[3].activate();
                        break;
                }
            }
        }
			

    }

	public void incrementPower(string powerUpColour)
	{
		recentUpgrade = powerUpColour;
		switch (powerUpColour) {
		case "green":
                if(yellowUpgradeNum < maxUpgradeNum)
			        yellowUpgradeNum++;	
			break;
		case "red":
                if(purpleUpgradeNum < maxUpgradeNum)
			        purpleUpgradeNum++;
			break;
		}
	}

    private void stopAllGuns()
    {
        for(int i = 0; i < gunArray.Length; i++)
        {
            gunArray[i].stopShooting();
        }
    }
}
