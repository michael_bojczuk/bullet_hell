﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		switch (LayerMask.LayerToName(other.gameObject.layer)) {
		case "PowerUp":
			Destroy (other.gameObject);
			this.GetComponent<UpgradeScript> ().incrementPower (other.GetComponent<PowerUp> ().powerUpColour);
			break;

		}

	}


}
