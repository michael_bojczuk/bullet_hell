﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laserScript : MonoBehaviour, gunInterface {

    public float length, laserDamage;
    public GameObject laserHead, laserMid;

    private bool firing;
    private bool active;
    private GameObject[] laserArray;
    private GameObject laserHeadObj;
	private float laserLength;
    private GameObject hitTarget;

	// Use this for initialization
	void Start () {
        laserArray = new GameObject[21];
        firing = false;
        active = false;
		laserLength = 0.5f;
        laserHeadObj = Instantiate(laserHead);
        hitTarget = null;
		for(int i = 0; i < laserArray.Length; i++)
        {
            laserArray[i] = Instantiate(laserMid);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
        if(active)
        {
            firing = Input.GetButton("Fire1");
        }
        if(firing)
        {

			for (int i = 0; i < laserArray.Length; i++) {
				laserArray [i].transform.position = new Vector3 (10, 10, 0);
				laserHeadObj.transform.position = new Vector3 (10, 10, 0);
			}
			int count = 0;
			float pos = 0.5f;
			Vector2 hitPos;

			while (pos < laserLength) {
				laserArray [count++].transform.position = this.transform.position + new Vector3 (0, pos, 0);
				pos += 0.5f;
			}
			laserArray [count].transform.position = this.transform.position + new Vector3 (0, laserLength, 0);
			laserLength += 0.08f;
			if (laserLength > 10) {
				laserLength = 10;
			}
			RaycastHit2D hit = Physics2D.Raycast (this.transform.position, Vector2.up, laserLength + 0.25f, 1<<LayerMask.NameToLayer("Enemy"));
			if (hit) {
				hitPos = hit.point;
				laserHeadObj.transform.position = hitPos;
				laserLength = Vector3.Distance (hitPos, this.transform.position + new Vector3(0,0.25f,0));
                hitTarget = hit.transform.gameObject;
			}
            else
            {
                hitTarget = null;
            }

        }
        else
        {
			for(int i = 0; i < laserArray.Length; i++)
            {
                laserArray[i].transform.position = new Vector3(10, 10, 0);
                laserHeadObj.transform.position = new Vector3(10, 10, 0);
				laserLength = 0.5f;
            }
        }
	}

    void FixedUpdate()
    {
        if(hitTarget!= null)
        {
            hitTarget.GetComponent<alienSpawner>().takeDamage(laserDamage);
        }
    }

	public void activate()
	{
		active = true;
	}

	public void deactivate()
	{
		active = false;
	}

	public void setSpeed(float speed)
	{
	}
    
    public void stopShooting()
    {
        firing = false;
    }
}
