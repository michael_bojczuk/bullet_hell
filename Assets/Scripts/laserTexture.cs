﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laserTexture : MonoBehaviour {

    float scrollSpeed = 0.5f;
    float offset = 0;
 
 void Update()
    {
        offset += (float)(Time.deltaTime * scrollSpeed) / 10.0f;
        this.GetComponent<SpriteRenderer>().material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }
}
