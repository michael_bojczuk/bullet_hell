﻿using UnityEngine;
using System.Collections;

public class alienSpawner : MonoBehaviour {

    public Vector3 spawnPosition;
    public float spawnTime;
	public GameObject[] path;
    public int pathPoints;
    public int[] pathTime;
	public float maxHP;

    private bool spawned;
    private int currentPathPoint;
    private float t, key;
	private float currentHp;
    private Vector3 startPos;

    //explosions
    public GameObject explosion;

	// Use this for initialization
	void Start () {

        spawned = false;
        t = 0;
		currentHp = maxHP;
	}

	void Update () {
        //Spawn ship after spawn time
        if(Time.timeSinceLevelLoad >= spawnTime && spawned == false)
        {
            this.transform.position = spawnPosition;
            spawned = true;
            startPos = spawnPosition;
        }

        if(spawned)
        {
            //Find time key
            if (pathTime[currentPathPoint] != 0)
            {
                key = t / pathTime[currentPathPoint];
            }
            else
            {
                key = 1;
            }
            //Lerp along the path
			this.transform.position = (startPos + (path[currentPathPoint].transform.position - startPos) * key);
            t += Time.deltaTime;
            if(t >= pathTime[currentPathPoint])
            {
                startPos = this.transform.position;
                t = 0;
				if (currentPathPoint < pathPoints - 1) {
					currentPathPoint++;
				} else {
                    Destroy (this.gameObject);
				}
            }
        }

    }

	public void takeDamage(float damage)
	{
		currentHp -= damage;
		if (currentHp <= 0) {
            Score.score += 1;
			gotDestroyed ();
		}
	}

	public void gotDestroyed()
	{
		if(this.GetComponent<dropPowerup>().dropsPowerup)
			this.GetComponent<dropPowerup> ().dropPowerUp ();

        Instantiate(explosion, transform.position, transform.rotation);
        
        Destroy (this.gameObject);
	}
}
